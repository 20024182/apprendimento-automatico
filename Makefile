# Executables
latexmk = latexmk

# Main file name
MAIN = apprendimento-automatico
MASTER = main
WORKING_DIR = src/

PRESENTATION = handout beamer

all: $(PRESENTATION) article

$(PRESENTATION):
	cd $(WORKING_DIR) && \
	mkdir -vp ../out && \
	rm -vf $(MASTER).presentation.pdf && \
	$(latexmk) --halt-on-error --file-line-error -lualatex -usepretex='\providecommand{\outputformat}{$@}' $(MASTER).presentation.tex && \
	cp -v $(MASTER).presentation.pdf ../out/$(MAIN)-$@.pdf

article: beamer
	cd $(WORKING_DIR) && \
	$(latexmk) --halt-on-error --file-line-error -lualatex $(MASTER).$@.tex && \
	cp -v $(MASTER).$@.pdf ../out/$(MAIN)-$@.pdf

check: all
	curl -sL 'https://dev.verapdf-rest.duallab.com/api/validate/3b/' \
	  -H 'Accept: application/json' \
	  -H "X-File-Size: $(shell stat --printf='%s' "./out/$(MASTER).presentation.pdf")" \
	  -F file="@./out/$(MASTER).presentation.pdf" \
	  -F sha1Hex="$(shell sha1sum "./out/$(MASTER).presentation.pdf" | cut -d ' ' -f 1)" \
	  -H 'DNT: 1' \
	  -H 'Cache-Control: no-cache' \
	  -H 'Sec-GPC: 1' | jq -r -e '.report.jobs[].validationResult[].compliant'
	# TODO:
	#  - check if all figures have fonts embedded
	#  - does out/*-article.pdf require PDF/A or PDF/X?
	#  - check if out/*-handout.pdf is a PDF/X

figures:
	# TODO: create a zip file with all figures

.PHONY: check
