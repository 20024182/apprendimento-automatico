# For CPU-only users who are concerned about package size
tensorflow-cpu;platform_system!="Darwin" or platform_machine!="arm64"
tensorflow;platform_system=="Darwin" and platform_machine=="arm64"

# Common deps.
-r requirements-common.txt