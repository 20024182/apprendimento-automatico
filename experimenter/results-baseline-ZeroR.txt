Tester:     weka.experiment.PairedCorrectedTTester -G 3,4,5 -D 1 -R 2 -S 0.05 -V -result-matrix "weka.experiment.ResultMatrixPlainText -mean-prec 3 -stddev-prec 2 -col-name-width 0 -row-name-width 25 -mean-width 0 -stddev-width 0 -sig-width 0 -count-width 5 -show-stddev -show-avg -print-col-names -print-row-names -enum-col-names"
Analysing:  Percent_correct
Datasets:   1
Resultsets: 9
Confidence: 0.05 (two tailed)
Sorted by:  -
Date:       01/02/24 17.40


Dataset                   (1) rules.ZeroR ''  | (2) bayes.NaiveB (3) trees.J48 '- (4) trees.J48 '- (5) trees.J48 '- (6) trees.J48 '- (7) lazy.IBk '-K (8) functions.Mu (9) functions.Si
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Data_for_UCI_named        (10)   63.800(0.00) |   83.260(0.51) v   85.376(0.76) v   84.924(0.92) v   84.924(0.92) v   85.376(0.76) v   86.664(0.70) v   86.628(0.82) v   81.424(0.73) v
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Average                          63.800       |   83.260           85.376           84.924           84.924           85.376           86.664           86.628           81.424        
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                      (v/ /*) |          (1/0/0)          (1/0/0)          (1/0/0)          (1/0/0)          (1/0/0)          (1/0/0)          (1/0/0)          (1/0/0)


Key:
(1) rules.ZeroR '' 48055541465867954
(2) bayes.NaiveBayes '' 5995231201785697655
(3) trees.J48 '-R -J -N 5 -Q 1 -M 2' -217733168393644444
(4) trees.J48 '-R -J -N 10 -Q 1 -M 2' -217733168393644444
(5) trees.J48 '-R -B -J -N 10 -Q 1 -M 2' -217733168393644444
(6) trees.J48 '-R -B -J -N 5 -Q 1 -M 2' -217733168393644444
(7) lazy.IBk '-K 10 -W 0 -X -E -I -A \"weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.EuclideanDistance -R first-last\\\"\"' -3080186098777067172
(8) functions.MultilayerPerceptron '-L 0.3 -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a' -5990607817048210779
(9) functions.SimpleLogistic '-I 0 -M 500 -H 50 -W 0.0' 7397710626304705059

