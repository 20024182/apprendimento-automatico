# Installation

## Download this repository
To install this repository and run the Jupyter notebooks on your machine, you will first need git, which you may already have.
Open a terminal and type `git` to check.
If you do not have git, you can download it from [git-scm.com](https://git-scm.com/).

Next, clone this repository by opening a terminal and typing the following commands (do not type the first `$` on each line, it's just a convention to show that this is a terminal prompt, not something else like Python code):
```shell
$ cd $HOME  # or any other development directory you prefer
$ git clone --depth=1 -b mscoco https://gitlab.di.unipmn.it/20024182/apprendimento-automatico.git
$ cd apprendimento-automatico
```
If you do not want to install git, you can instead download [apprendimento-automatico-mscoco.zip](https://gitlab.di.unipmn.it/20024182/apprendimento-automatico/-/archive/mscoco/apprendimento-automatico-mscoco.zip), unzip it, rename the resulting directory to `apprendimento-automatico` and move it to your development directory.

## Install Python
Next, you will need Python 3 and a bunch of Python libraries.
At the time of writing, Python 3.13 [is not supported](https://github.com/tensorflow/tensorflow/issues/78774) by TensorFlow and `tensorboard-plugin-profile` [is build for Python 3.11 and below](https://pypi.org/project/tensorboard-plugin-profile/#files).

To easily manage multiple Python versions, you can install [Pyenv](https://github.com/pyenv/pyenv).
Then run
```shell
pyenv install -v 3.11.11
```

## Create the virtual Environment
Next, make sure you're in the `apprendimento-automatico` directory and run the following commands.
pyenv will read a local version specified in an `.pyenv-version` file in this directory:
```shell
# See https://github.com/pyenv/pyenv/blob/master/COMMANDS.md#pyenv-local
pyenv local
```
The next command will create a new virtual environment that will contain every library you will need to run all the notebooks:
```shell
$ export PYTHONWARNINGS=always # Warn every time for CPython and iPython kernel
$ python3 -b -m venv --upgrade-deps '.venv'
```
Next, activate the new environment:
```shell
$ source ./.venv/bin/activate
```
Ensure `pip`, `setuptools`, and `wheel` are up to date:
```shell
$ python3 -b -m pip install --upgrade -vvv pip setuptools wheel
```
Finally, install all dependencies **for the appropriate runtime**:
```shell
$ python3 -b -m pip install --require-virtualenv -vvv -r ./requirements-gpu.txt
```

## Install the GPU Driver and Libraries
If you have a TensorFlow-compatible GPU card (NVIDIA&reg; card with [Compute Capability > 5.0](https://stackoverflow.com/a/71489432)), and you want TensorFlow to use it, then you should download the latest driver for your card from [nvidia.com](https://www.nvidia.com/Download/index.aspx?lang=en-us) and install it. 
You will also need NVIDIA&reg;'s CUDA and cuDNN libraries.

On Linux, you **probably** want to install a new IPython kernel for Jupyter.
Run the following commands to install a new [kernel spec](https://jupyter-client.readthedocs.io/en/latest/kernels.html#kernelspecs) file under `{sys.prefix}/share/jupyter/kernels`:
```shell
# Get the NVIDIA directory
$ NVIDIA_PATH="$(dirname "$(python -c 'import nvidia; print(nvidia.__file__)')")"

$ export JUPYTER_PLATFORM_DIRS=1 # https://jupyter-core.readthedocs.io/en/latest/changelog.html#migrate-to-standard-platform-directories

$ python3 -b -m ipykernel install --name 'python3-gpu' --display-name 'Python3 (GPU)' --sys-prefix \
    --env 'LD_LIBRARY_PATH' "$(find "${NVIDIA_PATH}" -type d -name '*lib' -prune -printf '%p:')\${LD_LIBRARY_PATH}" \
    --env 'PATH' "$(find "${NVIDIA_PATH}" -type d -name '*bin' -prune -printf '%p:')\${PATH}"
```
We also set two environment variables for the kernel.
In this way, we override system paths to use the CUDA libraries whose [versions are based on build components](https://github.com/tensorflow/tensorflow/blob/657d0bd08849f8702c43bb5798cb03588d2a81e0/tensorflow/tools/pip_package/setup.py#L152).
Furthermore, we avoid [this issue](https://github.com/tensorflow/tensorflow/issues/63362).

You will have to select it in the "Kernel > Change kernel..." menu in Jupyter every time you open a **new** notebook.
You're almost there!
You must add [`optirun`](https://en.wikipedia.org/w/index.php?title=Nvidia_Optimus&oldid=1211592386) before launching `python3`.
To do it, open `"${VIRTUAL_ENV}"/share/jupyter/kernels/python3-gpu/kernel.json` in a text editor and add `optirun` command as first item in the `argv` JSON array.

> **Note**
> Use the following to list all the kernels and their locations:
> ```shell
> $ python3 -b -m jupyter kernelspec list
> ```

## Start JupyterLab

You can now start JupyterLab like this:
```shell
$ python3 -b -m jupyter lab --ZMQChannelsWebsocketConnection.iopub_data_rate_limit=1000000000000
```
This should open up your browser, and you should see Jupyter's tree view with the contents of the current directory.
If your browser does not open automatically, visit [localhost:8888](http://localhost:8888/tree).

If you want to access JupyterLab remotely, you can run it like this:
```shell
$ python3 -b -m jupyter lab --ZMQChannelsWebsocketConnection.iopub_data_rate_limit=1000000000000 --ip 0.0.0.0 --no-browser
```

### Test your installation

Run all cells in [this notebook](http://localhost:8888/notebooks/deeplearning/check-hardware.ipynb).

## Update This Project and its Libraries
For this, open a terminal, and run:
```shell
$ cd $HOME # or whatever development directory you chose earlier
$ cd apprendimento-automatico # go to this project's directory
$ git pull
```
If you get an error, it's probably because you modified something.
In this case, before running `git pull` you will first need to commit your changes.
I recommend doing this in your own branch, or else you may get conflicts:
```shell
$ git checkout -b my_branch # you can use another branch name if you want
$ git add -u
$ git commit -m "describe your changes here"
$ git checkout main
$ git pull
```
Next, let's update virtual environment:
```shell
$ python3 -b -m pip install --require-virtualenv -vvv --upgrade-strategy 'eager' --upgrade -r ./requirements-gpu.txt
```
Next, we start JupyterLab:
```shell
$ python3 -b -m jupyter lab --ZMQChannelsWebsocketConnection.iopub_data_rate_limit=1000000000000
```

## Troubleshooting

### `TF-TRT Warning: Could not find TensorRT`

It's just a warning. Ignore it.

### NUMA node read from SysFS had negative value (-1)

To get rid of this non-fatal warning, use this command:
```shell
$ for a in /sys/bus/pci/devices/*; do echo 0 | sudo tee -a $a/numa_node; done
```
For details, go to [this answer](https://stackoverflow.com/a/44233285) in Stack Overflow.

### Not creating XLA devices, `tf_xla_enable_xla_devices` not set and XLA device creation not requested

See [this](https://github.com/tensorflow/tensorflow/commit/403258a15c64b0a6fb8d0d40996702bd304446af).

### `_load_jupyter_server_extension` function was not found in <module 'jupyter_lsp'>

> **Note**
> This warning appears only if `PYTHONWARNINGS=always` is set.

At the moment of writing, `jupyter-lsp` defines [`load_jupyter_server_extension`](https://github.com/jupyter-lsp/jupyterlab-lsp/blob/b2eb9b174172ce7050b515b082b86fc873264d79/python_packages/jupyter_lsp/jupyter_lsp/serverextension.py#L51-L85).
But Jupyter Server has required `_load_jupyter_server_extension` [since 2020](https://github.com/jupyter-server/jupyter_server/blob/c67a46befdcc972436150e9876b5d25d3c33ea56/CHANGELOG.md?plain=1#L2384).
So it shows [a warning](https://github.com/jupyter-server/jupyter_server/blob/c67a46befdcc972436150e9876b5d25d3c33ea56/jupyter_server/extension/utils.py#L42-L50).