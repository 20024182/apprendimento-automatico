{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b762abd95e8b21a1",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "# Image denoising\n",
    "\n",
    "<a href=\"https://colab.research.google.com/drive/\" target=\"_parent\"><img src=\"https://colab.research.google.com/assets/colab-badge.svg\" alt=\"Open In Colab\"/></a>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "377c2fb8705f2154",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TF: increase logging verbosity of C++\n",
    "%env TF_CPP_MAX_VLOG_LEVEL=1000\n",
    "%env TF_DUMP_GRAPH_PREFIX=/tmp/dump"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f1a82d86d3e3a97b",
   "metadata": {},
   "outputs": [],
   "source": [
    "%env TF_CPP_LOG_THREAD_ID=1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3c95b4be0865e8b8",
   "metadata": {},
   "source": [
    "## Google Colab\n",
    "\n",
    "Colab usually filters Info and Warning logs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3c0b946092d39663",
   "metadata": {},
   "outputs": [],
   "source": [
    "if 'google.colab' in str(get_ipython()):   \n",
    "    !sudo apt clean all\n",
    "    !sudo apt update\n",
    "    !sudo apt -y full-upgrade\n",
    "    !sudo apt -y autoremove\n",
    "\n",
    "    !pip uninstall -y pydot-ng pydotplus\n",
    "    !pip install --upgrade pip setuptools wheel\n",
    "    !pip install --upgrade-strategy \"eager\" --upgrade tensorflow tensorboard-plugin-profile pydot kaggle --extra-index-url https://pypi.nvidia.com\n",
    "    !pip install --upgrade 'ipdb'\n",
    "    \n",
    "    import os\n",
    "    import signal\n",
    "\n",
    "    # Kill the process to restart with the updated versions.\n",
    "    os.kill(os.getpid(), signal.SIGTERM)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1f3d509695d8253c",
   "metadata": {},
   "source": [
    "If `pip` complains about packages dependency you can [ignore it](https://github.com/googlecolab/colabtools/issues/2657#issuecomment-1064562489).\n",
    "Furthermore, note that no extra packages are installed along with `tensorflow`.\n",
    "Indeed, Google Colab relies on [system CUDA](https://github.com/googlecolab/colabtools/issues/4491#issuecomment-2050014933).\n",
    "\n",
    "If a modal appears to restart runtime, **ignore it**.\n",
    "This notebook restarts runtime for you."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f48ba78b5dfbb21d",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## Setup\n",
    "\n",
    "This notebook uses the TensorFlow backend."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7639f00a877b1d25",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "%env KERAS_BACKEND=tensorflow"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba58ada7e8e14027",
   "metadata": {},
   "source": [
    "Let's start by defining some constants."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2082cec0df63458a",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "\n",
    "if 'google.colab' in str(get_ipython()):\n",
    "    PROJECT_ROOT_DIR = Path(get_ipython().run_line_magic(magic_name=\"pwd\", line=\"\"))\n",
    "else:\n",
    "    PROJECT_ROOT_DIR = Path(get_ipython().run_line_magic(magic_name=\"env\", line=\"VIRTUAL_ENV\")).parent\n",
    "\n",
    "# Path of directory where to store the data needed to restore the model\n",
    "BACKUP_DIR = PROJECT_ROOT_DIR / \"deeplearning/backup\"\n",
    "\n",
    "# A directory containing checkpoint file\n",
    "CHECKPOINT_PATH = PROJECT_ROOT_DIR / \"deeplearning\"\n",
    "\n",
    "LOGS_PATH = PROJECT_ROOT_DIR / \"deeplearning/logs/image-denoising\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7fdd5dea89bea9c2",
   "metadata": {},
   "source": [
    "### Configure GPU thread usage\n",
    "\n",
    "Set the thread mode to [`gpu_private`](https://github.com/tensorflow/tensorflow/blob/068cfff65dd7be5e1bcb1ef773dfe4eb6d50e06e/tensorflow/core/common_runtime/gpu/gpu_device.cc#L612-L656) to make sure that preprocessing does not steal all the GPU threads.\n",
    "In other words, if there are a lot of CPU-bound operations happening (e.g., `tf.data` pipeline does a lot of reads from the disk), it can create CPU contention, causing GPU _kernel_ (set of instructions) launches to be delayed.\n",
    "This, in turn, delays code getting executed on the GPU.\n",
    "With `TF_GPU_THREAD_MODE` variable, you can alleviate the delays on the GPU caused by CPU contention.\n",
    "More concretely, this variable controls how CPU threads are allocated to launch kernels on the GPU.\n",
    "In particular, `gpu_private` allocates two threads to launch kernels for the GPU. The number of threads can be changed by settings the `TF_GPU_THREAD_COUNT` variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aa06d76b15c317cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "%env TF_GPU_THREAD_MODE=gpu_private"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f81e4115a4639759",
   "metadata": {},
   "source": [
    "### Max out the L2 cache\n",
    "\n",
    "When working with NVIDIA&reg; GPUs, execute the code snippet below before the training loop to max out the L2 fetch granularity to 128 bytes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36645ce25ee58e9f",
   "metadata": {},
   "outputs": [],
   "source": [
    "from cuda import cuda, cudart, nvrtc\n",
    "\n",
    "def _cudaGetErrorEnum(error):\n",
    "    if isinstance(error, cuda.CUresult):\n",
    "        err, name = cuda.cuGetErrorName(error)\n",
    "        return name if err == cuda.CUresult.CUDA_SUCCESS else \"<unknown>\"\n",
    "    elif isinstance(error, cudart.cudaError_t):\n",
    "        return cudart.cudaGetErrorName(error)[1]\n",
    "    elif isinstance(error, nvrtc.nvrtcResult):\n",
    "        return nvrtc.nvrtcGetErrorString(error)[1]\n",
    "    else:\n",
    "        raise RuntimeError('Unknown error type: {}'.format(error))\n",
    "\n",
    "def checkCudaErrors(result):\n",
    "    if result[0].value:\n",
    "        raise RuntimeError(\"CUDA error code={}({})\".format(result[0].value, _cudaGetErrorEnum(result[0])))\n",
    "    if len(result) == 1:\n",
    "        return None\n",
    "    elif len(result) == 2:\n",
    "        return result[1]\n",
    "    else:\n",
    "        return result[1:]\n",
    "\n",
    "checkCudaErrors(cudart.cudaDeviceSetLimit(cudart.cudaLimit.cudaLimitMaxL2FetchGranularity, 128))\n",
    "assert checkCudaErrors(cudart.cudaDeviceGetLimit(cudart.cudaLimit.cudaLimitMaxL2FetchGranularity)) == 128"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63f1fe664d7706af",
   "metadata": {},
   "source": [
    "### matplotlib mode\n",
    "\n",
    "To make plots using Matplotlib, you must first enable IPython's matplotlib mode.\n",
    "\n",
    "To do this, run the `%matplotlib` magic command to enable plotting in the current notebook.\n",
    "\n",
    "This magic takes an optional argument that specifies which MatPlotlib backend should be used.\n",
    "Most of the time, in the Notebook, you will want to use the `inline` backend, which will display _inline_ within frontends like Jupyter Lab, directly below the code cells that produced them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b52372bc857208f9",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1cf5325cdc72cd2e",
   "metadata": {},
   "source": [
    "See also [Plotting section](https://ipython.readthedocs.io/en/stable/interactive/plotting.html#matplotlib-magic) in IPython tutorial."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "136f895349bd1b55",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## Import packages\n",
    "\n",
    "We import a variety of packages that are used throughout the notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce8a3661fbabcbed",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from typing import Optional\n",
    "\n",
    "import keras\n",
    "import numpy as np\n",
    "\n",
    "if 'google.colab' in str(get_ipython()):\n",
    "    assert keras.__version__ >= '3.0.0', \"Keras version >= 3.x required\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da57500f0febe6e8",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## Visualization tools\n",
    "\n",
    "To easily visualize the images, we also define the following function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1eef737a39a3d3f2",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from textwrap import fill\n",
    "\n",
    "def plot_image(image_data: np.ndarray, title: str = None, ax = plt, width: Optional[int] = None):\n",
    "    \"\"\"\n",
    "    Plot image from image tensor.\n",
    "    \n",
    "    :param image_data: 3D image tensor. [height, width, channels].\n",
    "    :param title: Title to display in the plot.\n",
    "    :param ax: A single Axes object\n",
    "    :param width: The maximum width of wrapped lines\n",
    "    \"\"\"\n",
    "    ax.imshow(image_data)\n",
    "    if title is not None:\n",
    "        if width is None:\n",
    "            ax.set_title(title)\n",
    "        else:\n",
    "            ax.set_title(fill(title, width=width))\n",
    "    ax.axis(\"off\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "583b4404d791d2d0",
   "metadata": {},
   "source": [
    "## Load the dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "81b42fa387737164",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "authors": [
   {
    "name": "Lorenzo Ferron"
   }
  ],
  "kernelspec": {
   "display_name": "Python3 (GPU)",
   "language": "python",
   "name": "python3-gpu"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.11"
  },
  "title": "Image denoising"
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
