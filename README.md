<div align="center">
<div>
<a href="../-/jobs/artifacts/mscoco/raw/out/apprendimento-automatico-beamer.pdf?job=compile"><img src="https://img.shields.io/badge/download-presentation-green?style=for-the-badge" alt="presentation"></a>
<a href="../-/jobs/artifacts/mscoco/raw/out/apprendimento-automatico-article.pdf?job=compile"><img src="https://img.shields.io/badge/download-article-green?style=for-the-badge" alt="article"></a>
<a href="../-/jobs/artifacts/mscoco/raw/out/apprendimento-automatico-handout.pdf?job=compile"><img src="https://img.shields.io/badge/download-handout-green?style=for-the-badge" alt="handout"></a>
</div>
</div>

# Apprendimento automatico

Presentazione e codice sorgente Python per l'esame di Apprendimento Automatico (6 + 3 CFU) del prof. Portinale (A.A.2021/22).

Nonché ottimo esempio, per studenti e insegnanti, di uso della classe LaTeX [`beamer`](https://ctan.org/pkg/beamer).
Qualora si volesse preparare le slide per le lezioni di un intero corso è necessario dividerle in `\lecture`.

Si precisa che _non è stata curata_ la "versione articolo", perché non richiesta per l'esame.

## TOC
- Read the [detailed installation instructions](INSTALL.md).
- Go to [machinelearning](machinelearning) module.
- Go ot [deeplearning](deeplearning) module.
