# Machine Learning

| Tool         | version                               |
|--------------|---------------------------------------|
| Java         | OpenJDK Runtime Environment 11.0.21+9 |
| SciKit-Learn | 1.4.0, 1.5.0                          |
| WEKA         | 3.9.7-SNAPSHOT                        |

## Datasets used

A gentle introduction to datasets used.

### PCA & Naïve Bayes (NB)

The [Electrical Grid Stability Simulated Data](https://archive.ics.uci.edu/dataset/471/electrical+grid+stability+simulated+data) is the outcome of a simulation experiment using a four-node star grid.
In the simulation experiment, three key input variables of the model were selected (each allowed to vary independently) for each of the four grid participants.
These key input variables include: (i) $P_{j}$, $j = 1, 2, 3, 4$ (referred to as `p1`, ..., `p4` in the simulated data set) describing the mechanical power produced (for $j = 1$) or consumed (for $j = 2, 3, 4$); (ii) $\tau_{j}$, $j = 1, 2, 3, 4$ (referred to as `tau1`, ..., `tau4` in the simulated data set) describing reaction time of each grid participant to an electricity price change; and (iii) $\gamma_{j}$, $j = 1, 2, 3, 4$ (referred to as `g1`, ..., `g4` in the simulated data set) which is a coefficient proportional to price elasticity for each grid participant. 
All the previously listed key input variables, except for $P_{1}$ ($P_1 = −(P_2 + P_3 + P_4)$), are sampled as uniform distributions throughout their respective feasible spaces to initialize and launch the simulations (10000 simulation runs were performed).
The model’s output variable (referred to as `stab` in the simulated data set), i.e., the stability metric (ranging from −0.0808 to +0.1094), is quantified such that a negative value of that metric indicates that the grid is stable, whereas a positive value indicates that the grid is unstable.
In the simulated data set the stab-variable is accompanied by a `stabf` label of the grid stability—a categorical attribute taking values from a two-element set: "stable" for `stab` < 0 and "unstable" for `stab` > 0.
The details regarding the simulation experiment are presented [here](https://dbis.ipd.kit.edu/download/DSGC_simulations.pdf).

### _k_-nearest neighbors

The [Combined-Cycle Power Plant (CCPP)](https://archive.ics.uci.edu/dataset/294/combined+cycle+power+plant) dataset consists of time-series data collected over a period of six years, from 2006 to 2011.
This dataset contains 9568 samples of hourly measurements of various features such as ambient temperature (AT), exhaust vacuum (V), ambient pressure (AP), and relative humidity (RH), as well as the net hourly electrical energy output (PE) of the power plant.
The publicly available CCPP dataset consisted of five sheets, each containing 9568 data items that had been randomly shuffled.
