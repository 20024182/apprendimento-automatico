#!/usr/bin/env bash
#
# To use the Input Mono fonts, you must agree to the terms of the license.
#
#   https://input.djr.com/license/
#
# SEPARATE PUBLISHING LICENSE does not allow for redistribution.
# See also [this comment](https://github.com/NixOS/nixpkgs/pull/19080#issuecomment-251136003)

set +e

# Constant
readonly RED='\033[0;31m'
readonly NC='\033[0m' # No Color

function err() {
  echo -e "${RED}[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $*${NC}" >&2
}

function is_command_installed() {
  if ! command -v "${1}" &> /dev/null
  then
    err "${1} could not be found"
    exit 1
  fi
}

function main() {
  is_command_installed 'curl'
  is_command_installed 'unzip'

  cd "$(dirname "$0")" || exit 1

  # the served font is built dynamically, according to the query string.
  # To test it: https://input.djr.com/preview/?size=11&language=python&theme=solarized-dark&family=InputMono&width=100&weight=200&line-height=1.2&a=0&g=0&i=serif&l=serif&zero=0&asterisk=height&braces=0&preset=default&customize=please
  curl -L 'https://input.djr.com/build/?fontSelection=fourStyleFamily&regular=InputMonoCompressed-ExtraLight&italic=InputMonoCompressed-ExtraLightItalic&bold=InputMonoCompressed-Medium&boldItalic=InputMonoCompressed-MediumItalic&a=0&g=0&i=serif&l=serif&zero=0&asterisk=height&braces=0&preset=default&line-height=1.2&accept=I+do&email=' \
    -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:134.0) Gecko/20100101 Firefox/134.0' \
    -H 'Accept: application/zip' \
    -H 'DNT: 1' \
    -H 'Sec-GPC: 1' \
    -H 'Upgrade-Insecure-Requests: 1' \
    -H 'Sec-Fetch-Dest: document' \
    -H 'Sec-Fetch-Mode: navigate' \
    -H 'Sec-Fetch-Site: same-origin' \
    -H 'Sec-Fetch-User: ?1' \
    -H 'Cache-Control: no-cache' \
    -H 'TE: trailers' \
    -o 'InputMono.zip'

  unzip -j ./InputMono.zip 'Input_Fonts/Input/*.ttf'

  rm -rfv ./InputMono.zip

  cd - || exit 1
}

main "$@"
exit 0
